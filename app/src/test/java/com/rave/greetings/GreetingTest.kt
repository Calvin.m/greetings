package com.rave.greetings

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

class GreetingTest {

    //top level declaration and initialization
    private val greeting = Greeting()

    @Test
    @DisplayName("Test if method returns name with string")
    fun requirement1() {
        //Given
        val name = "Bob"
        //When
        val message: String = greeting.greet(name)
        //Then
        Assertions.assertEquals("Hello, $name.", message)
    }

    @Test
    @DisplayName("Test when name is null 😎")
    fun requirement2() {
        //Given
        val name: String? = null
        //When
        val message: String = greeting.greet(name)
        //Then
        Assertions.assertEquals("Hello, my friend.", message)
    }

    @Test
    @DisplayName("Test when someone is yelling")
    fun requirement3() {
        //Given
        val name: String = "CALVIN"
        //When
        val message: String = greeting.greet(name)
        println("message: $message")
        //Then
        Assertions.assertEquals("HELLO $name!", message)
    }

    @Test
    @DisplayName("test of two names")
    fun requirement4() {
        //Given
        val name = listOf("Jill", "Jane")
        //When
        val message: String = greeting.greet(name)
        //Then
        Assertions.assertEquals("Hello, ${name[0]} and ${name[1]}.", message)
    }

    @Test
    @DisplayName("Test more than 2 people")
            /*
            * Handle an arbitrary number of names as input.
            * When name represents more than two names, separate
            * them with commas and close with an Oxford comma and "and".
            * For example, when name is ["Amy", "Brian", "Charlotte"],
            * then the method should return the
            * string "Hello, Amy, Brian, and Charlotte."
            * */
    fun requirement5() {
        //Given
        val name = listOf("Jill", "Jane", "Bob", "Alice")
        //When
        val message: String = greeting.greet(name)
        //Then
        Assertions.assertEquals("Hello, ${name[0]}, ${name[1]}, ${name[2]}, and ${name[3]}.", message)
    }
    @Test
    @DisplayName("handle multi names plus shouting")
    /*Allow mixing of normal and shouted names by separating the response into two greetings. For example, when name is ["Amy", "BRIAN", "Charlotte"], then the method should return the string "Hello, Amy and Charlotte. AND HELLO BRIAN!"*/
    fun requirement6() {
        //Given
        val name = listOf("Amy", "BRIAN", "Charlotte")
        //When
        val message: String = greeting.greet(name)
        //Then
        Assertions.assertEquals("Hello, ${name[0]} and ${name[2]}. AND HELLO ${name[1]}!", message)
    }

    @Test
    @DisplayName("test")
    /*If any entries in name are a string containing a comma, split it as its own input. For example, when name is ["Bob", "Charlie, Dianne"], then the method should return the string "Hello, Bob, Charlie, and Dianne.".*/
    fun requirement7() {
        //Given
        val name = listOf("Bob", "Charlie, Diane")
        //When
        val message: String = greeting.greet(name)
        //Then
        Assertions.assertEquals("Hello, Bob, Charlie, and Dianne.", message)
    }
}


//Handle two names of input. When name is an array of two names
// (or, in languages that support it, varargs or a splat),
// then both names should be printed.
// For example, when name is ["Jill", "Jane"],
// then the method should return the string "Hello, Jill and Jane."

/*
* Requirement 1

Write a method greet(name) that interpolates name in a simple greeting. For example, when name is "Bob", the method should return a string "Hello, Bob.".

Requirement 2

Handle nulls by introducing a stand-in. For example, when name is null, then the method should return the string "Hello, my friend."

Requirement 3

Handle shouting. When name is all uppercase, then the method should shout back to the user. For example, when name is "JERRY" then the method should return the string "HELLO JERRY!"


Requirement 4

Handle two names of input. When name is an array of two names (or, in languages that support it, varargs or a splat), then both names should be printed. For example, when name is ["Jill", "Jane"], then the method should return the string "Hello, Jill and Jane."

Requirement 5

Handle an arbitrary number of names as input. When name represents more than two names, separate them with commas and close with an Oxford comma and "and". For example, when name is ["Amy", "Brian", "Charlotte"], then the method should return the string "Hello, Amy, Brian, and Charlotte."

Requirement 6

Allow mixing of normal and shouted names by separating the response into two greetings. For example, when name is ["Amy", "BRIAN", "Charlotte"], then the method should return the string "Hello, Amy and Charlotte. AND HELLO BRIAN!"

Requirement 7

If any entries in name are a string containing a comma, split it as its own input. For example, when name is ["Bob", "Charlie, Dianne"], then the method should return the string "Hello, Bob, Charlie, and Dianne.".

Requirement 8

Allow the input to escape intentional commas introduced by Requirement 7. These can be escaped in the same manner that CSV is, with double quotes surrounding the entry. For example, when name is ["Bob", "\"Charlie, Dianne\""], then the method should return the string "Hello, Bob and Charlie, Dianne.".
* */