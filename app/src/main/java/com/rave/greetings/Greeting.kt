package com.rave.greetings

class Greeting {
    fun greet(name: String?): String {
        return if (name.isStringAllCaps) "HELLO $name!" else "Hello, ${name ?: "my friend"}."
    }

    fun greet(names: List<String>): String {

//        var arr = listOf<String>()
//        println(arr)
//        var splitNames = names.forEach {
//            if (it.contains(",")) {
//                println(it)
//                var splitArr = it.split(",")
//                println("splitArr")
//                println(splitArr)
//                arr.add(splitArr[0])
//                arr.add(splitArr[1])
//            } else {
//                println("it")
//                println(it)
//                arr.add(it)
//                println(arr)
//            }
//        }
        var newNames = names.map {
            if (it.contains(",")) {
                //TODO: returning an array, not two individual arrays
                it.split(",")
            } else {
                it
            }
        }
        println("=============")
        println("NAMES")
        println(newNames)
        println(newNames.size)
        println("============")


        return when (names.size) {
            0 -> greet(null)
            1 -> greet(names.first())
            2 -> greet(names.joinToString(separator = " and "))
            else -> {
                println("NAMES IN WHEN")
                println(names)
                val (allCapsList, nonCapsList) = names.partition { it.isStringAllCaps }
                val nonCapsResult = handleAllNonCapsNameListFormatting(nonCapsList)
                val capsResult = handleAllCapsNameListFormatting(allCapsList)
                nonCapsResult + capsResult
            }
        }
    }

    private fun handleAllNonCapsNameListFormatting(nonAllCapsNames: List<String>) : String {
        return if (nonAllCapsNames.size == 2) {
            greet(nonAllCapsNames)
        } else {
            nonAllCapsNames.reduceIndexed { index, acc, s ->
                if (nonAllCapsNames.lastIndex != index) "$acc, $s" else "$acc, and $s"
            }.run { greet(this) }
        }
    }

    private fun handleAllCapsNameListFormatting(allCapsNames: List<String>) : String {
        return if (allCapsNames.isNotEmpty()) {
            val reducedFormattedString = allCapsNames.reduceIndexed { index, acc, s ->
                if (allCapsNames.lastIndex != index) "$acc, $s" else "$acc, and $s"
            }
            " AND ${greet(reducedFormattedString)}"
        } else {
            ""
        }
    }
    private val String?.isStringAllCaps: Boolean
        get() = this?.toCharArray()?.all { it.isUpperCase() } ?: false
}